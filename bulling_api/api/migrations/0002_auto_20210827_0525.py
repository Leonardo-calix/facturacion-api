# Generated by Django 3.2.6 on 2021-08-27 05:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Detail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Discount',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=15)),
                ('value', models.FloatField()),
                ('active', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer', models.CharField(max_length=15)),
                ('date', models.DateField()),
                ('subTotal', models.FloatField()),
                ('total', models.FloatField()),
                ('isv', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=15)),
                ('image', models.URLField(max_length=300)),
                ('price', models.FloatField()),
                ('quantity', models.IntegerField()),
                ('active', models.BooleanField()),
                ('DiscountId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.discount')),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.DateField(max_length=15)),
                ('firstName', models.DateField(max_length=15)),
                ('email', models.EmailField(max_length=30)),
                ('password', models.CharField(max_length=45)),
                ('active', models.BooleanField()),
            ],
        ),
        migrations.DeleteModel(
            name='Products',
        ),
        migrations.AddField(
            model_name='invoice',
            name='UserId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.users'),
        ),
        migrations.AddField(
            model_name='detail',
            name='InvoiceId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.invoice'),
        ),
        migrations.AddField(
            model_name='detail',
            name='ProductId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.product'),
        ),
    ]
