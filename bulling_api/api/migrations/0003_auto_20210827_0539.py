# Generated by Django 3.2.6 on 2021-08-27 05:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20210827_0525'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstName', models.CharField(max_length=15)),
                ('lastName', models.CharField(max_length=15)),
                ('email', models.EmailField(max_length=30)),
                ('password', models.CharField(max_length=45)),
                ('active', models.BooleanField()),
            ],
        ),
        migrations.AlterField(
            model_name='discount',
            name='name',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='UserId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.user'),
        ),
        migrations.DeleteModel(
            name='Users',
        ),
    ]
