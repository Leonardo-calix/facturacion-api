from django.core.checks import messages
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from .models import Detail, Product, Discount, Invoice
import json

# Create your views here.
class ProductView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if id > 0:
            product = list(Product.objects.filter(pk=id).values())
            data = {'product': product[0]}
        else:
            products = list(Product.objects.select_related().values())
            data = {'products': products}

        return JsonResponse(data)

    def post(self, request):
        body = json.loads(request.body)
        discount = Discount.objects.get(pk=body['discount'])

        Product.objects.create(
            name=body['name'],
            image=body['image'],
            price=body['price'],
            quantity=body['quantity'],
            Discount_id=discount.id
        )

        return JsonResponse({'message': 'Producto creado'})


class Discountview(View):

    @ method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        discounts = list(Discount.objects.select_related().values())
        data = {'discounts': discounts}
        return JsonResponse(data)

class InvoiceView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):

        if id > 0:
            invoice = list(Invoice.objects.filter(pk=id).values())
            data = {'invoice': invoice[0]}
        else:
            invoices = list(Invoice.objects.values())
            data = {'invoices': invoices}

        return JsonResponse(data)

    def post(self, request):
        # print(request.body)
        body = json.loads(request.body)
        print(body)

        invoice = Invoice(
            customer=body['customer'],
            date=body['date'],
            subTotal=body['subTotal'],
            total=body['total'],
            discounts=body['discounts'],
            isv=body['isv']

        )

        invoice.save()

        return JsonResponse({'message': 'Factura ingresada con exito', 'invoice': invoice.id})

class DatailView(View):

    @ method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):

        if id > 0:
            detail = list(Detail.objects.filter(Invoice_id=id).values())
            data = {'detail': detail}
        else:
            details = list(Detail.objects.values())
            data = {'details': details}

        return JsonResponse(data)

    def post(self, request):
        # print(request.body)
        body = json.loads(request.body)
        print(body)

        datail = Detail(
            Invoice_id=body['invoiceId'],
            Product_id=body['productId'],
            quantity=body['quantity'],
        )

        datail.save()

        return JsonResponse({'message': 'Factura ingresada con exito'})
