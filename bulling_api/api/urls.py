from django.urls import path
from .views import ProductView, Discountview, InvoiceView, DatailView

urlpatterns = [
    path('products/', ProductView.as_view(), name='products-list'),
    path('products/<int:id>', ProductView.as_view(), name='product'),
    path('discounts/', Discountview.as_view(), name='discounts-list'),
    path('invoices/', InvoiceView.as_view(), name='invoices-list'),
    path('invoices/<int:id>', InvoiceView.as_view(), name='invoice'),
    path('details/', DatailView.as_view(), name='details-list'),
    path('details/<int:id>', DatailView.as_view(), name='detail'),



]
