from django.db import models
from django.contrib import admin
from .models import Product, Discount, Detail, Invoice

# Register your models here.
admin.site.register(Product)
admin.site.register(Discount)
admin.site.register(Detail)
admin.site.register(Invoice)

