from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.


class Discount(models.Model):
    name = models.CharField(max_length=20)
    value = models.FloatField()
    active = models.BooleanField()

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=15)
    image = models.URLField(max_length=300)
    price = models.FloatField()
    quantity = models.IntegerField()
    Discount = models.ForeignKey(Discount, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Invoice(models.Model):
    customer = models.CharField(max_length=15)
    date = models.DateField()
    subTotal = models.FloatField()
    total = models.FloatField()
    discounts = models.FloatField(default=0)
    isv = models.FloatField(default=0)

    def __str__(self):
        return self.customer


class Detail(models.Model):
    Invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    Product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return self.Invoice
